# I
# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов

# II
# Пишем игру ) Программа выбирает из диапазона чисел (1-100) случайное число и предлагает пользователю его угадать.
# Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать еще раз, пока он не угадает.
# Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить игру. N - закончить

# III
# Пользователь вводит строку произвольной длины. Написать функцию, которая должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - список слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:
#
# {
#
# "0": количество пробелов в строке
#
# "1": list слов из одной буквы
#
# "2": list слов из двух букв
#
# "3": list слов из трех букв
#
# и т.д ...
#
# "punctuation" : tuple уникальных знаков препинания
#
# }
import random as rnd

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

# I
def doSomething(arg1, arg2):
    try:
        if (arg1.isdigit() or isfloat(arg1)) and (arg2.isdigit() or isfloat(arg2)):
            arg1 = float(arg1)
            arg2 = float (arg2)
            return arg1 * arg2
        elif (arg1.isdigit() or isfloat(arg1)) or (arg2.isdigit() or isfloat(arg2)):
            dict = {}
            dict[arg1] = arg2
            return dict
        elif False == (arg1.isdigit() or isfloat(arg1)) or (arg2.isdigit() or isfloat(arg2)):
            return arg1 + " " + arg2
        else:
            return tuple(arg1, arg2)

    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        return message


# II
def simpleGame():

    rnd.seed()
    max_random_number = 3
    random_number = rnd.randint(0, max_random_number - 1)
    wish = "y"
    print(f"Program generates a random int from 0 to {max_random_number - 1}. Guess the number.")
    print("Enter number until you guess program random:")

    while wish == "y":
        user = input()
        try:
            if user.isdigit():
                user = int(user)

                if user == random_number:
                    print("Congratulations! Do you want try again? (y/n)")
                    wish = input().lower()

                    if wish == "y":
                        random_number = rnd.randint(0, max_random_number - 1)
                        print(f"So, let's rerun. Generated from 0 to {max_random_number - 1}")
                        continue
                    elif wish == "n":
                        print("Bye!")
                        return
                    else:
                        print("Wrong input")
                        return
            else:
                print("Wrong input format. Enter only whole numbers.")
                return

        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            return message

#III
def makeDict():
    print("Input string:")
    text = input()

    word = []
    punctuation = []
    text_lst = text.split(" ")
    key_lst = []

    # finding and sorting the keys for future dictionary
    for item in text_lst:
        key_lst.append(len(item))
    key_lst.sort()
    key_lst = set(key_lst)

    # making tuple with unique speacial characters
    for chara in text:
        if chara.isalnum() == False and chara != " " and chara != "":
            punctuation.append(chara)


    punctuation = set(punctuation)
    punctuation = tuple(punctuation)

    # counting spaces
    spaces = text.count(" ")

    # initial dictionary
    sample_dict = {
        "spaces": spaces,
        "punctuation": punctuation
    }

    for key in key_lst:
        for value in text_lst:
            if len(value) == key:
                # print("len(value) = ", len(value), " ", type(len(value)))
                # print("key = ", key, " ", type(key))
                word.append(value)
        sample_dict[str(key)] = word
        word = []

    print("\nFinal dictionary:\n")
    print("{:<10}    {:<15}".format("Key", "Value"""))
    for key, value in sample_dict.items():
        print("{:<10}     {:<15}".format(key, str(value)))

    return

def main():
    # I
    # print("Input any two arguments: ")
    # print("arg1 = ", end="")
    # arg1 = input()
    # print("arg2 = ", end="")
    # arg2 = input()
    # print("Function doSomething result: ",(doSomething(arg1, arg2)))

    # II
    #simpleGame()

    # III
    makeDict()


main()